var m = "I got 24 FaRenheit (24F), my height is 34 feet and 5 inches, my weight is 123lbs or 23 pounds 2'3\" 5'"

var weightUnits = ["lbs", "pounds", "pound", "lb"]
var inchUnits = ["inch", "inches", "\""]
var feetUnits = ["foot", "feet", "'", "ft"]
var temperatureUnits = ["f", "farenheit"]

var formattedWeightUnits = "lbs"
var formattedInchUnits = "\""
var formattedFeetUnits = "'"
var formattedTemperatureUnits = "F"


var allUnits = weightUnits.concat(inchUnits).concat(feetUnits).concat(temperatureUnits)


function convert(imperialTokens)
{
	var metricTokens = []
	for(var i = 0; i < imperialTokens.length; i += 1)
	{
		if (imperialTokens[i].includes(formattedWeightUnits))
		{
			metricTokens.push(toKg(imperialTokens[i].replace(formattedWeightUnits, "")))
			continue
		}
		if (imperialTokens[i].includes(formattedTemperatureUnits))
		{
			metricTokens.push(toCelsius(imperialTokens[i].replace(formattedTemperatureUnits, "")))
			continue
		}
		if (imperialTokens[i].includes(formattedFeetUnits))
		{
			var values = imperialTokens[i].split(formattedFeetUnits)
			metricTokens.push(toMetricDistance(values[0], values[1]))
			continue
		}
		if (imperialTokens[i].includes(formattedInchUnits))
		{
			metricTokens.push(toMetricDistance(0, imperialTokens[i].replace(formattedInchUnits, "")))
			continue
		}
		metricTokens.push("n/a") // Just in case something goes wrong, we still need to maintain the array order.
	}
	
	var finalTable = ""
	for(var i = 0; i < imperialTokens.length; i += 1)
	{
		finalTable += imperialTokens[i] + " = " + metricTokens[i] + "\n"
	}
	return finalTable
}


// Determines if a string has imperial units and returns an array 
// of all occurences formatted in a specific way.
// Possible outputs:
// 1lbs - weight in pounds
// 1F - temperature
// 1' - feet
// 1'1 - feet + inches
// 1" - inches
function getImperial(input)
{
	input = input.toLowerCase()

	var regex = /([0-9]*[.])?[0-9]+/gm
	var words = input.split(" ")

	var results = []

	for(var i = 0; i < words.length; i += 1)
	{
		var matches = words[i].match(regex)
		if (matches === null || !words[i].match(/^\d/))
		{
			continue
		}

		// Processing regular units.
		if (matches.length == 1)
		{
			var match = matches[0]

			// If the number and units are divided with a space,
			// join them back together.
			// Note that this check looks SPECIFICALLY for lone units without a number.
			if (
				i != words.length - 1
				&& !words[i + 1].match(regex)
				&& isUnit(words[i + 1], allUnits)
			)
			{
				words[i] += words[i + 1]
				console.log(words[i])
			}
			
			if (includesOne(words[i], weightUnits))
			{
				results.push(match + formattedWeightUnits)
				continue
			}

			if (includesOne(words[i], feetUnits))
			{
				results.push(match + formattedFeetUnits)
				continue
			}

			if (includesOne(words[i], inchUnits))
			{
				// If feet + inches are divided with a space, join them in a single entry.
				// Yes, this is a very naive check.
				if (results.length > 0 && results[results.length - 1].endsWith("'"))
				{
					results[results.length - 1] += match
				}
				else
				{
					results.push(match + formattedInchUnits)
				}
				continue
			}

			if (includesOne(words[i], temperatureUnits))
			{
				results.push(match + formattedTemperatureUnits)
				continue
			}
		}

		// Processing special snowflake feet + inches.
		if (matches.length == 2)
		{
			var feetInchRegex = /([0-9]*[.])?[0-9]+'([0-9]*[.])?[0-9]/
			if (words[i].match(feetInchRegex))
			{
				results.push(matches[0] + "'" + matches[1])
				continue
			}
		}
	}
	return results;
}


function includesOne(str, arr)
{
	for(var i = 0; i < arr.length; i += 1)
	{
		if (str.includes(arr[i]))
		{
			return true
		}
	}
	return false
}

function isUnit(str, arr)
{
	for(var i = 0; i < arr.length; i += 1)
	{
		if (str === arr[i])
		{
			return true
		}
	}
	return false
}

function toMetricDistance(feet, inches)
{
	var cm = feet * 30.48 + inches * 2.54

	if (cm > 300)
	{
		return (cm / 100).toFixed(2) + "m"
	}
	return cm.toFixed(2) + "cm"
}


function toCelsius(f)
{
	return ((f - 32) * (5.0 / 9.0)).toFixed(2) + "C"
}


function toKg(lbs)
{
	return (lbs / 2.205).toFixed(2) + "kg"
}


exports.convert = convert
exports.getImperial = getImperial
