const env = require('dotenv')
const Discord = require('discord.js')

const client = new Discord.Client(
	{ 
		intents: 
		[
			Discord.GatewayIntentBits.Guilds, 
			Discord.GatewayIntentBits.MessageContent,
			Discord.GatewayIntentBits.GuildMessageReactions,
			Discord.GatewayIntentBits.GuildMessages,
			Discord.GatewayIntentBits.GuildEmojisAndStickers,
			Discord.GatewayIntentBits.DirectMessages
		] 
	}
)

const shitpostMinDelay = 20
const shitpostMaxDelay = 100
var shitpostCounter = 0
const foxeMinDelay = 120 * 60 * 1000 // 2 hours
const foxeMaxDelay = 240 * 60 * 1000 // 4 hours

const clearCommandCounterDelay = 1 * 60 * 1000 // 1 minute
const maxManualCommands = 20
var manualCommandCounter = 0

const foxe = require("./commands/foxe")
const shitpost = require("./commands/shitpost")
const fact = require("./commands/fact")
const help = require("./commands/help")
const chatbot = require("./commands/chatbot")
const gif = require("./commands/post_gif")
const gondola = require("./commands/gondola")
const god = require("./commands/god")
const bottalk = require("./commands/bottalk")
const imperial = require("./imperial")
const br = require("./commands/br/br")

const ask = require("./commands/ask")
const inspire = require("./commands/inspire")
const insult = require("./commands/insult")
const angry = require("./commands/angry")

const dalle = require("./commands/dalle")
const save = require("./commands/save")
const vx = require("./commands/vx")
const anime = require("./commands/anime")
const prank = require("./commands/prank")

const protectedThreadWatcher = require("./thread_protection/watcher")

const ai = require("./commands/aidungeon/ai")


env.config() // Dotenv stuff.

client.on(
	"ready", 
	() => {
		console.log(`Foxe in. | ${client.user.tag}`)

		client.user.setActivity('yiff', { type: 'WATCHING' })
		//protectedThreadRestore.run(client)

		console.log("Running on servers:")
		var guilds = client.guilds.cache.map(g => g)
		for(var i = 0; i < guilds.length; i += 1)
		{
			console.log(guilds[i].name + ": " + guilds[i].id)
		}

		//prank.init(client)
	}
)

const commands = 
{
	"foxe": foxe,
	"shitpost": shitpost,
	"help": help,
	"fact": fact,
	"ai": ai,
	"gif": gif,
	"god": god,
	"bottalk": bottalk,
	"royale": br,
	"ask": ask,
	"insult": insult,
	"angry": angry,
	"inspire": inspire,
	"dalle": dalle,
	"save": save,
	"vx": vx,
	"anime": anime,
	"gondola": gondola
	//"prank": prank
}

var lastMessage = null

client.on(
	'messageCreate', 
	async msg => 
	{
		try
		{
			if (!msg.author.bot && msg.guild)
			{
				lastMessage = msg
			}

			const messageContent = msg.content
			const commandPrefix = '!'

			if (messageContent.toLowerCase().includes("foxlet"))
			{
				msg.react("737375713229013073")
			}

			if (messageContent.toLowerCase().includes("thogbot") && Math.random() <= 0.3)
			{
				msg.react("🇬")
				msg.react("🇦")
				msg.react("🇾")
				msg.react("🇧")
				msg.react("🇴")
				msg.react("🇹")
			}

			if (based(messageContent) && Math.random() <= 0.1)
			{
				await msg.channel.send("on what?")
			}

			checkImperial(msg)

			if (msg.mentions.has(client.user) && !messageContent.startsWith(commandPrefix))
			{
				try
				{
					chatbot.run(msg, msg.channel, null)
				}
				catch(e)
				{
					console.log(e)
				}
				return;
			}
			
			if (msg.author.bot || !msg.guild)
			{
				return
			}	

			if (messageContent === "no" && Math.random() <= 0.1)
			{
				if (Math.random() <= 0.1)
				{
					msg.channel.send("no")
				}
				else
				{
					msg.channel.send("yes")
				}
			}	

			if (messageContent === "yes" && Math.random() <= 0.1)
			{
				if (Math.random() <= 0.1)
				{
					msg.channel.send("yes")
				}
				else
				{
					msg.channel.send("no")
				}
			}	

			if (messageContent === "fox")
			{
				msg.channel.send("let")
			}

			if ( // I literally do not fucking care.
				messageContent.toLowerCase().includes("uwu") 
				|| messageContent.toLowerCase().includes("owo")
				|| messageContent.toLowerCase().includes("qwq")
			)
			{
				await msg.channel.send("<@" + msg.author.id + "> hang yourself")
			}

			if (msg.channel.id === process.env.AUTOPOST_CHANNEL_ID
				&& !msg.author.bot 
				&& !messageContent.startsWith(commandPrefix)
			)
			{
				console.log("Shitpost counter is " + shitpostCounter)
				shitpostCounter -= 1;
				if (shitpostCounter <= 0)
				{
					shitpostCounter = 9999
					setTimeout(autoShitpost, 2000)
				}
			}	

			if (!messageContent.startsWith(commandPrefix))
			{
				return
			}
		
			const messageArguments = messageContent.slice(commandPrefix.length).trim().split(/ +/g)
			const messageCommand = messageArguments.shift().toLowerCase()	

			console.log("Received command [" + messageCommand + "] from " + msg.author.tag)
		
			var command = commands[messageCommand]
			if (command !== undefined && command != "")
			{
				manualCommandCounter += 1	

				if (manualCommandCounter < maxManualCommands)
				{
					try
					{
						command.run(msg, msg.channel, messageArguments)
					}
					catch(e)
					{
						console.log(e)
					}
				}
			}
		}
		catch(e){}
	} 
)

client.login(process.env.DISCORD_TOKEN)
protectedThreadWatcher.init(client)


autoPostFoxes()
clearCommandCounter()

function clearCommandCounter()
{
	manualCommandCounter = 0
	setTimeout(clearCommandCounter, clearCommandCounterDelay);
}

function autoPostFoxes()
{
	var channel = client.channels.cache.get(process.env.AUTOPOST_CHANNEL_ID)
	if (channel === undefined)
	{
		return
	}
	console.log("Posting automatically.")
	foxe.run(null, channel, null)

	var timeout = lerp(foxeMinDelay, foxeMaxDelay, Math.random())
	console.log("Timeout set to " + timeout + "ms")
	setTimeout(autoPostFoxes, timeout)
}

function autoShitpost()
{
	console.log("Posting automatically. ")

	var ch = client.channels.cache.get(process.env.AUTOPOST_CHANNEL_ID)
	console.log(ch)
	
	if (Math.random() > 0.25)
	{
		shitpost.run(null, ch, null)
	}
	else
	if (Math.random() > 0.5)
	{
		if (Math.random() > 0.4)
		{
			if (Math.random() > 0.5)
			{
				shitpost.run(null, ch, null)
				if (Math.random() > 0.95)
				{
					for(var i = 0; i < 10; i += 1)
					{
						shitpost.run(null, ch, null)
					}
				}
			}
			else
			{
				if (Math.random() > 0.5)
				{
					inspire.run(null, ch, null)
				}
				else
				{
					if (Math.random() > 0.5)
					{
						insult.run(null, ch, null)
					}
					else
					{
						angry.run(null, ch, null)
					}
				}
			}
		}
		else
		{
			chatbot.run(lastMessage, lastMessage.channel, null)
		}
	}
	else
	{
		if (Math.random() > 0.5)
		{
			gondola.run(null, ch, null)
		}
		else
		{
			// Too lazy to implement proper choose function.
			if (Math.random() > 0.25)
			{
				gif.run(null, ch, null)
			}
			else
			{
				if (Math.random() > 0.25)
				{
					gif.run(null, ch, null)
				}
				else
				{
					foxe.run(null, ch, null)
				}
			}
		}
	}

	shitpostCounter = Math.floor(lerp(shitpostMinDelay, shitpostMaxDelay, Math.random()))
	console.log("Shitpost counter set to " + shitpostCounter)
}


function lerp(a, b, value)
{
	return b * value + a * (1 - value)
}


function based(input)
{
	var strippedInput = input.replace(/[^a-zA-Z ]/g, "").toLowerCase()
	console.log(strippedInput)

	if (!strippedInput.includes("based"))
	{
		return false;
	}
	var words = strippedInput.split(" ")
	for(var i = 0; i < words.length - 1; i += 1)
	{
		if (words[i] === "based" && words[i + 1] === "on")
		{
			return false
		}
	}
	return true
}

function checkImperial(msg)
{
	try
	{
		if (msg.author.bot)
		{
			return
		}
		
		var tokens = imperial.getImperial(msg.content)
	
		if (tokens.length == 0)
		{
			return
		}

		var table = imperial.convert(tokens)

		msg.channel.send(
			"In case someone wants a conversion from " 
			+ imperialInsults[Math.floor(Math.random() * imperialInsults.length)] + " units:\n```" + 
			table 
			+ "```"
		)
	}
	catch(e)
	{
		console.log("shidd :DDD " + e)
	}
}

var imperialInsults =
[
	"OOGA BOOGA",
	"IDIOT",
	"CAVEMAN",
	"SCHOOL SHOOTING",
	'"please be patient im autistic"',
	"REDDIT",
	"BLM",
	"WOMAN",
	"VEGAN",
	"SOY",
	"CLOWN",
	"MICROPENIS",
	"NON-BINARY"
]