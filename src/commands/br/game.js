const Player = require("./Player")
const logger = require("./roundLogger")
const strings = require("./strings")
const rng = require("./rng")

const initialState = "initial"
const lobbyState = "lobby"
const gameState = "game"

var currentState = initialState

var players = new Map()

var roundCount = 0

// Adds a new player to the match.
function addPlayer(name, id)
{
	if (currentState != lobbyState)
	{
		return "not_in_lobby"
	}

	var player = new Player.Player(name, id)
	players.set(id, player)

	if (bots && (id + "").startsWith("$bot_"))
	{
		player.isBot = true
	}

	if (startItems)
	{
		for(var i = 0; i < 3; i += 1)
		{
			player.items.push(strings.items[rng.irandom(strings.items.length)])
		}
	}

	return "success"
}


var startItems = false
var bots = false

// Initializes a new game and sets it into a lobby state.
function initGame(_startItems, _bots)
{
	startItems = _startItems
	bots = _bots
	players.clear()
	roundCount = 0
	currentState = lobbyState
}


// Starts a new match if there are enough players.
function startGame()
{
	if (players.size < 2)
	{
		currentState = initialState
		return "not_enough_players"
	}

	if (bots)
	{
		for(var i = 0; i < 5; i += 1)
		{
			addPlayer(strings.items[rng.irandom(strings.items.length)], "$bot_" + i)
		}
	}
	currentState = gameState

	return "success"
}


// Plays a round.
// actions - A map of all actions, with user id as key and action name as value.
function update(actions)
{
	if (currentState != gameState)
	{
		return "not_in_game"
	}
	roundCount += 1
	
	logger.clear()

	// Sorting by initiative.
	Array.from(players.values()).sort((a, b) => {return b.initiative - a.initiative}).forEach(
		(player) => 
		{
			console.log(player.initiative)
			
			player.update(players, actions.get(player.id))
		}
	)
	
	return logger.get()
}

// Logs all stats for all players.
function getStats()
{
	if (currentState != gameState)
	{
		return "not_in_game"
	}
	
	logger.clear()

	players.forEach(
		(player, id) => 
		{
			if (player.hp <= 0)
			{
				return
			}
			if (player.isBot)
			{
				logger.add("> [BOT] " + player.name + " " + player.hp + "/" + player.maxHp + "[♥]")
			}
			else
			{
				logger.add("> " + player.name + " " + player.hp + "/" + player.maxHp + "[♥]")
			}
			if (player.items.length > 0)
			{
				var itemsStr = "items: "
				for(var i = 0; i < player.items.length; i += 1)
				{
					itemsStr += "[" + player.items[i] + "]"
					if (i != player.items.length - 1)
					{
						itemsStr += ", "
					}
				}
				logger.add(itemsStr)
			}

		}
	)

	var newPlayers = new Map()
	var humanPlayers = new Map()

	players.forEach(
		(p, id) => 
		{
			if (p.hp <= 0)
			{
				var str = p.name + " is dead"
				logger.add(	"["+str.toUpperCase()+"]")
				return
			}
			if (!p.isBot)
			{
				humanPlayers.set(id, p)
			}
			newPlayers.set(id, p)
		}
	)
	players = newPlayers

	if (humanPlayers.size == 1)
	{
		currentState = initialState
		logger.add(Array.from(humanPlayers.values())[0].name +" is the winner!")
	}
	if (humanPlayers.size <= 0)
	{
		currentState = initialState
		logger.add("Everybody is dead! ULTRA MURDER!")
	}
	return logger.get()
}

function getPlayerInfo(id)
{
	var player = players.get(id)
	
	if (player === undefined)
	{
		return null
	}
	return "> " + player.name + " " + player.hp + "/" + player.maxHp + "[♥]\n" +
		".initiative: " + player.initiative + "\n" +
		".ferocity:   " + player.ferocity + "\n" +
		".wits:       " + player.wits + "\n" +
		".charm:      " + player.charm + "\n"
}

function isInGame()
{
	return currentState == gameState
}

function isFree()
{
	return currentState == initialState
}

function stopGame()
{
	currentState = initialState
	players = new Map()
	roundCount = 0
}

// API
exports.initGame = initGame
exports.update = update
exports.startGame = startGame
exports.addPlayer = addPlayer
exports.getStats = getStats
exports.getPlayerInfo = getPlayerInfo
exports.isInGame = isInGame
exports.isFree = isFree
exports.stopGame = stopGame
exports.getRoundCount = function()
{
	return roundCount
}

