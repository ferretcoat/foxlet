const updateInterval = 5

exports.MessageAlarm = function(message, time, messageText, triggerAction)
{
	this.message = message
	this.messageText = messageText
	this.triggerAction = triggerAction

	this.currectUpdateInterval = 0

	this.getTimerMessage = function(insert)
	{
		return "```js\n" + insert + "\n```"
	}

	this.timer = time

	this.message.edit(messageText + this.getTimerMessage(this.timer + " seconds left"))
	setTimeout(() => this.update(), 1000)

	this.update = function()
	{
		this.timer -= 1
		this.currectUpdateInterval -= 1

		if (this.currectUpdateInterval <= 0)
		{
			this.message.edit(messageText + this.getTimerMessage(this.timer + 1 + " seconds left"))

			this.currectUpdateInterval = updateInterval
		}
		
		if (this.timer > 0)
		{
			setTimeout(() => this.update(), 1000)
		}
		else
		{
			var report = new Map()

			this.message.reactions.cache.forEach(
				(v, k) => 
				{
					v.users.cache.forEach(
						(uv, uk) =>
						{
							console.log(uv.username + "#" + uv.discriminator)
							if (!uv.bot)
							{
								report.set(uv.username + "#" + uv.discriminator, k)
							}
						}
					)

				}
			)

			this.triggerAction(report)

			this.message.edit(messageText + this.getTimerMessage("TIME IS UP"))
		
		}
	}
}
