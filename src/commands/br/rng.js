
exports.random = function()
{
	return Math.random()
}

exports.irandom = function(value)
{
	return Math.floor(value * Math.random())
}


exports.irandomRange = function(minValue, maxValue)
{
	return Math.floor(minValue + Math.random() * (maxValue - minValue))
}


exports.choose = function(values)
{
	return values[Math.floor(Math.random() * values.length)]
}
