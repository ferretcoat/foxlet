const rng = require("./rng")
const logger = require("./roundLogger")
const strings = require("./strings")
const itemActions = require("./itemActions")

const baseDamage = 40

exports.attack = function(player, players)
{
	var target = getRandomPlayer(player, players)

	if (player.friends.indexOf(target.id) != -1)
	{
		// TODO: Possibly filter out friends entirely?
		logger.add(substituteString("{P1} remembers {P2} and instead of attacking each other they make out.", player.name, target.name))
		return
	}

	if (player.roll(0.5 + 0.05 * player.ferocity - 0.05 * target.charm))
	{
		var attackString = getRandomString(strings.attack) + getRandomString(strings.attackHits)
		
		logger.add(substituteString(attackString, player.name, target.name))
		var dmg = baseDamage * (player.friends.length + 1)
			+ rng.irandomRange(-player.ferocity, player.ferocity) * 3
			
		if (dmg <= 0)
		{
			dmg = 1
		}

		target.hp -= dmg
		logEffect(target.name + " takes " + dmg + " damage")
	}
	else
	{
		var attackString = getRandomString(strings.attack) + getRandomString(strings.attackMisses)
		
		logger.add(substituteString(attackString, player.name, target.name))
	}
}


exports.help = function(player, players)
{
	var target = getRandomPlayer(player, players)

	if (player.roll(0.5 + 0.05 * player.charm))
	{
		logger.add(substituteString(getRandomString(strings.friendshipSuccess), player.name, target.name))

		player.hasAdvantage = true
		
		target.friends.push(player.id)
		player.newFriends.push(target.id)
		logEffect("Both " + player.name + " and " + target.name + " will get +1 to damage on their next turn")
		logEffect("Both " + player.name + " and " + target.name + " will not be able to harm each other on their next turn")
	}
	else
	{
		logger.add(substituteString(getRandomString(strings.friendshipFailure), player.name, target.name))
	}
}

exports.stealItem = function(player, players)
{
	var target = getRandomPlayerWithItems(player, players)

	if (target == null)
	{
		console.log("Nobody has any items!")
		exports.help(player, players)
		return
	}

	if (player.roll(0.5 + 0.05 * player.charm))
	{
		var item = getRandomString(target.items)
	
		// Removing the item.
		var index = target.items.indexOf(item);
		if (index > -1) 
		{
			target.items.splice(index, 1);
		}
		
		logger.add(substituteString(getRandomString(strings.friendshipSuccess), player.name, target.name))
		logger.add(substituteString("{P2} gifts {P1} his [" + item + "]!", player.name, target.name))
		
		logEffect(player.name + " now has a " + item)
		
		itemActions.use(item, player, players)
	}
	else
	{
		logger.add(substituteString(getRandomString(strings.friendshipFailure), player.name, target.name))
	}
}



exports.rest = function(player, players)
{
	if (player.roll(0.6 + 0.05 * player.wits))
	{
		logger.add(substituteString(getRandomString(strings.restSuccess), player.name, ""))
		var heal = (baseDamage + 5 * rng.irandom(player.wits)) / 2
		player.hp += heal
		if (player.hp > player.maxHp)
		{
			player.hp = player.maxHp
		}
		logEffect(player.name + " regains " + heal +" hp")
	}
	else
	{
		logger.add(substituteString(getRandomString(strings.restFailure), player.name, ""))
	}
}

exports.findItem = function(player, players)
{
	if (player.roll(0.5 + 0.05 * player.wits))
	{
		var item = getRandomString(strings.items)
		
		logger.add(substituteString("After some dumpster diving, {P1} finds a [" + item + "] in a perfectly good condition! What a lucky day!", player.name, ""))
		player.items.push(item)
		logEffect(player.name + " now has a " + item)
	}
	else
	{
		logger.add(substituteString("{P1} storms through the garbage, but doesn't find anything!", player.name, ""))
	}
}

exports.use = function(player, players)
{
	var item = getRandomString(player.items)
	
	// Removing the item.
	var index = player.items.indexOf(item);
	if (index > -1) 
	{
		player.items.splice(index, 1);
	}

	itemActions.use(item, player, players)
}


//============================================

function getRandomPlayer(player, players)
{
	playerArray = Array.from(players.values()).filter(
		function(item)
		{ 
			return item != player; 
		}
	);

	return playerArray[rng.irandom(playerArray.length)]
}

function getRandomPlayerWithItems(player, players)
{
	playerArray = Array.from(players.values()).filter(
		function(item)
		{ 
			return item != player && item.items.length > 0; 
		}
	);

	if (playerArray == 0)
	{
		return null
	}

	return playerArray[rng.irandom(playerArray.length)]
}

function getRandomString(arr)
{
	return arr[rng.irandom(arr.length)]
}


function substituteString(str, p1, p2)
{
	return str.replace("{P1}", "{" + p1 + "}").replace("{P2}", "{" + p2 + "}")
}

function logEffect(str)
{
	logger.add("["+str.toUpperCase()+"]")
}