const rng = require("./rng")
const logger = require("./roundLogger")
const game = require("./game")
const strings = require("./strings")

var actions = 
[
	fortifyMaxHp,
	charm,
	rerollIntiative,
	fortifyWits,
	fortifyCharm,
	fortifyFerocity,
	russianRoulette,
	suddenDeath,
	prickle,
	swap,
	gift,
	god,
	rerollHp
]


exports.use = function(item, player, players)
{
	var action = rng.choose(actions)
	action(item, player, players)
}


function fortifyMaxHp(item, player, players)
{
	var hpGain = 30 + 5 * rng.irandom(player.wits)
	player.hp += hpGain
	player.maxHp += hpGain
	if (player.maxHp > 200)
	{
		player.maxHp = 200;
	}
	if (player.hp > player.maxHp)
	{
		player.hp = player.maxHp
	}

	logger.add(substituteString("{P1} absorbs the [" + item + "]'s power and becomes one point closer to achieving godhood.", player.name))
	logEffect(player.name + "`s max hp has been increased by " + hpGain)
}


function charm(item, player, players)
{
	player.hasAdvantage = true
	players.forEach(
		(p) => p.friends.push(player.id)
	)

	logger.add(substituteString("{P1} tears the [" + item + "] into lots of tiny pieces and gifts them to all other players.", player.name))
	logEffect(player.name + " has advantage and nobody can harm him on their next turn")
}


function rerollIntiative(item, player, players)
{
	player.initiative = rng.irandomRange(10, 20)

	logger.add(substituteString("[" + item + "] opens a spacetime rift which allows {P1} to alter reality and change the order of his turn!", player.name))
	logEffect(player.name + "`s initiative is now " + player.initiative)
}


function fortifyCharm(item, player, players)
{
	player.charm += 1

	logger.add(substituteString("{P1} sticks [" + item + "] up his butthole which makes him a bit more handsome than before!", player.name))
	logEffect(player.name + "`s charm is now " + player.charm)
}

function fortifyWits(item, player, players)
{
	player.wits += 1

	logger.add(substituteString("{P1} tears down [" + item + "] right then and there, learning all of its inner workings in the process!", player.name))
	logEffect(player.name + "`s wits is now " + player.wits)
}

function fortifyFerocity(item, player, players)
{
	player.ferocity += 1

	logger.add(substituteString("{P1} beats up his [" + item + "] as hard as he can. He seems more muscular than before!", player.name))
	logEffect(player.name + "`s ferocity is now " + player.ferocity)
}

function gift(item, player, players)
{
	players.forEach(
		(p) => p.items.push(strings.items[rng.irandom(strings.items.length)])
	)

	logger.add(substituteString("[" + item + "] turns out to be a box of nice presents!.", player.name))
	logEffect("everyone gets an item")
}

// CHAOTIC ITEMS

function suddenDeath(item, player, players)
{
	players.forEach(
		(p) => p.hp = 1
	)

	logger.add(substituteString("SUDDEN DEATH! The [" + item + "] blows up in a massive nuclear explosion, severily harming everyone and everything around it!!!.", player.name))
	logEffect("everyone`s hp has dropped to 1")
}

function prickle(item, player, players)
{
	players.forEach(
		(p) => p.hp -= 30
	)
	player.hp += 30

	logger.add(substituteString("{P1} takes the sharp end of [" + item + "] and prickles everybody!.", player.name))
	logEffect("everyone except " + player.name + " take 30 points of damage")
}

function rerollHp(item, player, players)
{
	player.hp = rng.irandom(player.maxHp) + 1

	logger.add(substituteString("{P1} takes his chances and asks [" + item + "] to let him start from a blank slate.", player.name))
	logEffect(player.name + "'s hp is now " + player.hp)
}

function god(item, player, players)
{
	for(var i = 0; i < 100; i += 1)
	{
		player.newFriends.push(player)
	}
	logger.add(substituteString("[" + item + "] uncovers true the power of friendship for {P1}. He feels AMPLIFIED.", player.name))
	logEffect("For the next round, " + player.name + " has a damage boost")
}

function swap(item, player, players)
{
	var target = getRandomPlayer(player, players)

	var initiative = target.initiative
	var hp = target.hp
	var maxHp = target.maxHp
	var items = target.items
	var wits = target.wits
	var charm = target.charm
	var ferocity = target.ferocity

	target.initiative = player.initiative
	target.hp = player.hp
	target.maxHp = player.maxHp
	target.items = player.items
	target.wits = player.wits
	target.charm = player.charm
	target.ferocity = player.ferocity

	player.initiative = initiative
	player.hp = hp
	player.maxHp = maxHp
	player.items = items
	player.wits = wits
	player.charm = charm
	player.ferocity = ferocity

	logger.add(substituteString("{P1} uses the quantum energy of [" + item + "] to swap his body with {P1}'s!.", player.name, target.name))
	logEffect(player.name + " and " + target.name + " have swapped all their stats")
}


function russianRoulette(item, player, players)
{
	var shuffledPlayers = Array.from(players.values())
	shuffledPlayers = shuffle(shuffledPlayers)


	var bullet = rng.irandomRange(0, shuffledPlayers.length)
	logger.add(substituteString("The [" + item + "] turns out to be a functioning " + shuffledPlayers.length + "-round revolver with a single bullet! Everybody gathers on a circle and tries their luck.", player.name))
	shuffledPlayers.forEach(
		(p) => 
		{
			if (bullet < 0)
			{
				return
			}
			logger.add(p.name + ", are you feeling lucky?")
			if (bullet == 0)
			{
				logger.add(substituteString(".BANG! The [" + item + "] shoots right through {P1}`s head splattering his brains everywhere!!!", p.name, p.name))
				logEffect(p.name + " is dead")
				p.hp = 0
				bullet = -1
				return
			}
			else
			{
				logger.add("*click*")
			}
			bullet -= 1
		}
	)

}


function substituteString(str, p1, p2)
{
	return str.replace("{P1}", "{" + p1 + "}").replace("{P2}", "{" + p2 + "}")
}

function logEffect(str)
{
	logger.add("["+str.toUpperCase()+"]")
}

function shuffle(a) 
{
	var j, x, i;
	for (i = a.length - 1; i > 0; i--) 
	{
			j = Math.floor(Math.random() * (i + 1));
			x = a[i];
			a[i] = a[j];
			a[j] = x;
	}
	return a;
}


function getRandomPlayer(player, players)
{
	playerArray = Array.from(players.values()).filter(
		function(item)
		{ 
			return item != player; 
		}
	);

	return playerArray[rng.irandom(playerArray.length)]
}