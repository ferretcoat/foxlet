var roundLog = ""


function get()
{
	return roundLog
}

function add(logMessage)
{
	roundLog += logMessage + "\n"
}

function clear()
{
	roundLog = ""
}


exports.get = get
exports.add = add
exports.clear = clear
