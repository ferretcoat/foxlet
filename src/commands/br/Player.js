const rng = require("./rng")
const logger = require("./roundLogger")
const actions = require("./playerActions")

const baseMaxHp = 100


exports.Player = function(name, id)
{
	this.isBot = false
	
	this.name = name
	this.id = id

	this.maxHp = 100
	this.hp = this.maxHp

	this.items = []
	this.friends = []
	this.newFriends = []

	// Initiative governs the order of players turn.
	this.initiative = rng.irandomRange(1, 20)
	// Ferocity influences the chance to perform a successful aggressive action.
	this.ferocity = 1
	//Wits influence the chance to perform a successful passive action.
	this.wits = 1
	// Charm influences the chance to perform a successful friendly action.
	this.charm = 1

	// Distributing stats.
	for(var i = 0; i < 6; i += 1)
	{
		var r = rng.irandom(3)
		if (r == 0)
		{
			this.ferocity += 1
		}
		if (r == 1)
		{
			this.wits += 1
		}
		if (r == 2)
		{
			this.charm += 1
		}
	}

	this.update = function(players, action)
	{
		if (action === "aggressive")
		{
			this.aggressiveAction(players)
			this.clearTempStats()
			return
		}
		if (action === "passive")
		{
			this.passiveAction(players)
			this.clearTempStats()
			return
		}
		if (action === "friendly")
		{
			this.friendlyAction(players)
			this.clearTempStats()
			return
		}
		if (action === "use")
		{
			if (this.items.length > 0)
			{
				this.useAction(players)
			}
			else
			{
				logger.add("{" + this.name + "} realizes he doesn't have any items!")
				this.randomAction(players)
			}

			this.clearTempStats()
			return
		}
		this.randomAction(players)
		this.clearTempStats()
	}


	this.clearTempStats = function() 
	{
		this.friends = this.newFriends
		this.newFriends = []
	}


	this.aggressiveAction = function(players)
	{
		actions.attack(this, players)
	}


	this.useAction = function(players)
	{
		actions.use(this, players)
	}


	this.passiveAction = function(players) 
	{
		var action = rng.choose(
			[
				// Needs lambdas here, otherwise we somehow are executing this shit in global context.
				(t, p) => actions.rest(t, p), 
				(t, p) => actions.findItem(t, p), 
			]
		)
		action(this, players)

	}


	this.friendlyAction = function(players) 
	{
		var action = rng.choose(
			[
				// Needs lambdas here, otherwise we somehow are executing this shit in global context.
				(t, p) => actions.help(t, p), 
				(t, p) => actions.stealItem(t, p), 
			]
		)
		action(this, players)
	}


	this.randomAction = function(players) 
	{
		if (this.items.length > 0)
		{
			var action = rng.choose(
				[
					// Needs lambdas here, otherwise we somehow are executing this shit in global context.
					(p) => this.aggressiveAction(p), 
					(p) => this.passiveAction(p), 
					(p) => this.friendlyAction(p),
					(p) => this.useAction(p),
				]
			)
		}
		else
		{
			var action = rng.choose(
				[
					// Needs lambdas here, otherwise we somehow are executing this shit in global context.
					(p) => this.aggressiveAction(p), 
					(p) => this.passiveAction(p), 
					(p) => this.friendlyAction(p),
				]
			)
		}
		action(players)
	}

	this.hasAdvantage = false

	this.roll = function(baseChance)
	{
		var roll = rng.random()
		
		var result = (roll < baseChance)

		if (!result && this.hasAdvantage)
		{
			console.log("Advantage! Making another roll...")
			roll = rng.random()
			this.hasAdvantage = false
			result = (roll < baseChance)
		}
		return result
	}
}

