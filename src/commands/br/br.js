const game = require('./game')
const MessageAlarm = require("./MessageAlarm")
const { help } = require('./playerActions')

var actions = new Map()

var joinMessage
var joinTimer = 10

var joinAlarm
var roundAlarm

var starterId = 0

async function run(msg, channel, args)
{

	if (args == null || args.length == 0)
	{
		await printHelp(channel)
		return
	}

	var argString = ""
	for(var i = 0; i < args.length; i += 1)
	{
		argString += args + " "
	}

	var startItems = false
	var bots = false
	if (argString.includes("start-items"))
	{
		startItems = true
	}
	if (argString.includes("bots"))
	{
		bots = true
	}

	action = args[0]
	if (action === "start")
	{
		if (game.isFree())
		{
			game.initGame(startItems, bots)
			starterId = msg.author.id

			joinMessage = await channel.send(welcomeMessage)
			joinMessage.react("737375713229013073")
			joinAlarm = new MessageAlarm.MessageAlarm(
				joinMessage, 
				30,
				welcomeMessage,
				async (report) => 
				{
					if (report.size > 1)
					{
						await addAllPlayers(report, channel)
						await startGame(channel)
					}
					else
					{
						await channel.send("There should be at least two players!")
						game.stopGame()
					}
				}
			)
		}

		return
	}

	if (action === "stats")
	{
		var info = game.getPlayerInfo(msg.author.username + "#" + msg.author.discriminator)

		if (info != null)
		{
			await channel.send("```css\n" + info + "\n```")
		}
		return
	}

	if (action === "stop")
	{
		if (game.isInGame())
		{
			if (msg.author.id === starterId)
			{
				game.stopGame()
				await channel.send("Relentless murder is finally over!")
			}
			else
			{
				await channel.send("Only a player who started the game can stop it!")
			}
		}
		return
	}

	await printHelp(channel)

}

exports.run = run

async function addAllPlayers(playerMap, channel)
{
	playerMap.forEach(
		async (emojiId, playerId) =>
		{
			console.log("player: " + playerId + " | emoji:" + emojiId)
			var username = playerId.split("#")[0]
			console.log("Adding player with id " + playerId + " and name:" + username)
			game.addPlayer(username, playerId)
			var info = game.getPlayerInfo(playerId)
			await channel.send("```css\n" + info + "\n```")
		}
	)
}

async function startGame(channel)
{
	game.startGame()

	setTimeout(() => printLogs([], channel, false), 3000)
}


var roundPrompt = `
THE BATTLE IS ON! 
Pick an action for the next round:
⚔ - aggressive action
🔍 - passive action
🍆 - friendly action
🐍 - use item
-----------------------------------------
!royale stats - see your player's info
`

async function update(channel)
{
	if (!game.isInGame())
	{
		return
	}
	console.log(actions)
	var logs = game.update(actions)

	await printLogs(splitLogs(logs), channel, true)
}

async function printLogs(logArray, channel, printStats)
{
	if (!game.isInGame())
	{
		return
	}
	
	if (logArray.length == 0)
	{
		setTimeout(
			async () =>
			{
				if (printStats)
				{
					await channel.send("```css\n" + game.getStats() + "\n```")
				}

				setTimeout(
					async () =>
					{

						actions = new Map()

						if (!game.isInGame())
						{
							return
						}

						var numberedRound = "[ROUND " + (game.getRoundCount() + 1) + "]\n" + roundPrompt
						var roundMessage = await channel.send(numberedRound)
						roundMessage.react("⚔")
						roundMessage.react("🔍")
						roundMessage.react("🍆")
						roundMessage.react("🐍")

						roundAlarm = new MessageAlarm.MessageAlarm(
							roundMessage, 
							15,
							numberedRound,
							async (report) => 
							{
								report.forEach(
									(emote, playerId) =>
									{ 

										console.log(emote + " + " + playerId)

										if (emote == "⚔")
										{
											actions.set(playerId, "aggressive")
										}
										if (emote == "🔍")
										{
											actions.set(playerId, "passive")
										}
										if (emote == "🍆")
										{
											actions.set(playerId, "friendly")
										}
										if (emote == "🐍")
										{
											actions.set(playerId, "use")
										}
										
									}
								)
								// Add player input
								update(channel)
							}
						)
					},
					3000
				)
			},
			3000
		)
	}
	else
	{
		setTimeout(
			async () =>
			{
				await channel.send("```css\n" + logArray[0] + "\n```")
				logArray.shift() // Removing top element.
				await printLogs(logArray, channel, true)
			}, 
			3000
		)
	}
}


function splitLogs(logs)
{
	var lines = logs.split("\n")
	var logArray = []
	var buffer = ""
	for(var i = 0; i < lines.length; i += 1)
	{
		if (lines[i].charAt(0) != "[" && buffer != "")
		{
			logArray.push(buffer)
			buffer = ""
		}
		if (lines[i] != "")
		{
			buffer += lines[i] + "\n"
		}
	}
	if (buffer != "")
	{
		logArray.push(buffer)
		buffer = ""
	}

	return logArray
}

async function printHelp(channel)
{
	await channel.send(
`
===FOXLET ROYALE=== 
The rules are simple: kill all your friends and don't die yourself! 
Type \`!royale start\` to start a new match.
Additional arguments:
- bots - Adds 5 bots to the match. Whoever survives the most is the winner!
- start-items - Adds 3 items to each player at the start of the game.
-----------------------------------------
Each round you can choose an action:
⚔ - aggressive action
	1. \`Attack other player. Your base damage is always 1.\`
🔍 - passive action 
	1. \`Look for items.\`
	2. \`Regain 1hp.\`
🍆 - friendly action
	1. \`Convince other players to help you out. This boosts both your and their damage, and prevents them from attacking you for 1 round.\`
	2. \`If another player has an item, he will gift it to you, and you will use it instantly.\`
🐍 - use item
	1. \`Use an item. If you don't have any, you will perform a random action instead.\`
-----------------------------------------
Each action has a chance to success or fail. The chance is determined by your stats:
**ferocity** - Boosts the chance of an **aggressive** action.
**wits** - Boosts the chance of a **passive** action.
**charm** - Boosts the chance of a **friendly** action.
!royale stats - see your player's current info
-----------------------------------------
Additional commands:

!royale stop - aborts current game (available only to a player who started the game)
`
	)
}

var welcomeMessage = 
`
\`\`\`css
                  WELCOME TO


    █████▒▒█████  ▒██   ██▒ ██▓    ▓█████▄▄▄█████▓
  ▓██   ▒▒██▒  ██▒▒▒ █ █ ▒░▓██▒    ▓█   ▀▓  ██▒ ▓▒
  ▒████ ░▒██░  ██▒░░  █   ░▒██░    ▒███  ▒ ▓██░ ▒░
  ░▓█▒  ░▒██   ██░ ░ █ █ ▒ ▒██░    ▒▓█  ▄░ ▓██▓ ░ 
  ░▒█░   ░ ████▓▒░▒██▒ ▒██▒░██████▒░▒████▒ ▒██▒ ░ 
   ▒ ░   ░ ▒░▒░▒░ ▒▒ ░ ░▓ ░░ ▒░▓  ░░░ ▒░ ░ ▒ ░░   
   ░       ░ ▒ ▒░ ░░   ░▒ ░░ ░ ▒  ░ ░ ░  ░   ░    
   ░ ░   ░ ░ ░ ▒   ░    ░    ░ ░      ░    ░      
             ░ ░   ░    ░      ░  ░   ░  ░        

 ██▀███   ▒█████ ▓██   ██▓ ▄▄▄       ██▓    ▓█████ 
▓██ ▒ ██▒▒██▒  ██▒▒██  ██▒▒████▄    ▓██▒    ▓█   ▀ 
▓██ ░▄█ ▒▒██░  ██▒ ▒██ ██░▒██  ▀█▄  ▒██░    ▒███   
▒██▀▀█▄  ▒██   ██░ ░ ▐██▓░░██▄▄▄▄██ ▒██░    ▒▓█  ▄ 
░██▓ ▒██▒░ ████▓▒░ ░ ██▒▓░ ▓█   ▓██▒░██████▒░▒████▒
░ ▒▓ ░▒▓░░ ▒░▒░▒░   ██▒▒▒  ▒▒   ▓▒█░░ ▒░▓  ░░░ ▒░ ░
  ░▒ ░ ▒░  ░ ▒ ▒░ ▓██ ░▒░   ▒   ▒▒ ░░ ░ ▒  ░ ░ ░  ░
  ░░   ░ ░ ░ ░ ▒  ▒ ▒ ░░    ░   ▒     ░ ░      ░   
   ░         ░ ░  ░ ░           ░  ░    ░  ░   ░  ░
                  ░ ░                              
\`\`\`

Join by reacting to this message.
`