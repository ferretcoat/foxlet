const http = require('../http')

async function run(msg, channel, args)
{
	http.getHttps(
		"https://boredhumans.com/yelling.php",
		async d => {
			var pageFilter = /<B>I Am Mad At You:<\/B><BR><BR>.*<b<br>/

			var filteredInsult = (pageFilter.exec(d) + "").replace("<B>I Am Mad At You:<\/B><BR><BR>", "").replace("<b<br>", "")


			console.log("Sending insult: " + filteredInsult)

			await channel.send(filteredInsult)
		}
	)
}

//run(null, null, null)

exports.run = run