const http = require('./../http')
const fs = require('fs')
const client = require('https')
const path = require('path')

const channelId = "1091351558660313149"
const emoteId = "850355232768393216"
const voteLimit = 4

async function run(msg, channel, args)
{
	if (msg.author.id == process.env.OWNER)
	{
		//msg.channel.send("Fuck off you can't submit pictures to yourself lol.")
		//return
	}
	if (msg.channel.id !== channelId && msg.guild.id === "699037523501908049")
	{
		channel.send("This command only works in <#1091351558660313149> lol fuck you.")
		return
	}

	var urls = getUrlsFromMessage(msg)

	if (urls.length == 0)
	{
		await msg.channel.send("No attachments! Use `!prank` command and attach an image!")
		return
	}

	var vote = await msg.channel.send(
		{
			files: 
			[
				{
					attachment: urls[0],
					name: 'prank.png'
				}
			],
			content:"Vote for this image by reacting with <:emote:" + emoteId + ">. Requested by " + msg.author.username,
		}
	)
	vote.react(emoteId)
	await msg.delete()
}

function init(client)
{
	client.on(
		'messageReactionAdd', 
		async (reaction_orig, user) => 
		{
			if (reaction_orig.message.channel.id != channelId)
			{
				return
			}

			var count = 0;
			reaction_orig.message.reactions.cache.forEach(
				(v, k) => 
				{
					if (k == emoteId)
					{
						count = v.count
					}
				}
			)
			console.log(count)
			if (count == voteLimit)
			{
				sendPic(reaction_orig.message)
			}
		}
	);
	console.log("Initialized!")
}

async function sendPic(msg)
{
	var urls = getUrlsFromMessage(msg)

	if (urls.length == 0)
	{
		await msg.channel.send("Something went seriously wrong :(.")
		return
	}
	var username = msg.content.split("Requested by ")[1]

	var res = await msg.channel.send(
		{
			files: 
			[
				{
					attachment: urls[0],
					name: 'prank.png'
				}
			],
			content: "**This image will be sent straight to the foxe desk <:emote:" + emoteId + ">.** Image provided by " + username,
		}
	)

	var filename = "prank_" + Date.now() + path.extname(urls[0])
	var dirName = process.env.PRANK_DIR + path.dirname(filename)

	if (!fs.existsSync(dirName))
	{
		fs.mkdirSync(dirName)
	}
	await downloadFile(urls[0], dirName + "/" + filename)

	await msg.delete()
}


function getUrlsFromMessage(msg)
{
	var urls = []
	getUrlsFromAttachments(msg, urls)
	getUrlsFromText(msg.content, urls)
	return urls
}

function getUrlsFromAttachments(msg, urls)
{
	if (msg.attachments && msg.attachments.size > 0)
	{
		var attachments = msg.attachments.values();
		console.log(attachments)

		for (let [key, value] of msg.attachments)
		{
			if (isPicture(value.url))
			{
				urls.push(value.url)
			}
		}
	}
	return urls
}

function getUrlsFromText(messageContent, urls)
{
	if (messageContent.includes("https://"))
	{
		words = messageContent.replace("\n", " ").split(" ")
		for(var i = 0; i < words.length; i += 1)
		{
			if (words[i].includes("https://") && isPicture(words[i]))
			{
				urls.push(words[i])
			}
		}
	}
}


function isPicture(filename)
{
	return filename.endsWith(".png") || filename.endsWith(".jpg") || filename.endsWith(".jpeg")
}



function downloadFile(url, filepath) 
{
	return new Promise(
		(resolve, reject) => 
		{
			client.get(
				url, 
				(res) => 
				{
					if (res.statusCode === 200) 
					{
						res.pipe(fs.createWriteStream(filepath))
							.on('error', reject)
							.once('close', () => resolve(filepath))
					} 
					else
					{
						// Consume response data to free up memory
						res.resume();
						reject(new Error(`Request Failed With a Status Code: ${res.statusCode}`))
					}
				}
			)
		}
	)
}



exports.run = run
exports.init = init