const http = require('./../http')
const cleverbot = require("cleverbot-free");

var allowedBotMessages = 0;

async function run(msg, channel, args)
{
	console.log("Allowed messages: " + allowedBotMessages)
	if (msg.author.bot && allowedBotMessages <= 0)
	{
		return
	}
	else
	{
		allowedBotMessages -= 1
	}

	var request = stripMention(msg.content).trim()
	if (request === "")
	{
		return
	}

	cheekyChatbot(msg, request)
}

function stripMention(message)
{
	var regex = /(?:<@.+?>)/
	return message.replace(regex.exec(message), "").trim()
}

function setAllowedBotMessages(value)
{
	allowedBotMessages = value
}

exports.run = run
exports.setAllowedBotMessages = setAllowedBotMessages



var history = []
const maxHistoryLength = 200

function cheekyChatbot(msg, request)
{
	cleverbot(msg.content, history).then(
		response => 
		{
			try
			{
				if (response === undefined)
				{
					throw "fugg"
				}

				console.log("Chat request: " + request)
				console.log("Responding: " + response)
				setTimeout(
					async () => 
					{
						try
						{
							await msg.channel.send({ content: response, reply: { messageReference: msg.id } });
						}
						catch(e){}
					}, 
					300
				)

				// Adding context history.
				history.push(msg.content)
				history.push(response)
				while(history.length > maxHistoryLength)
				{
					history.shift();
				}

			}
			catch(e)
			{
				console.log("CHATBOT EXCEPTION: " + e.message)
				var dumpMessage = "I've taken a huge dump and missed your message, sorry."
				if (msg.author.bot)
				{
					dumpMessage = "<@" + msg.author.id + "> " + dumpMessage
				}

				setTimeout(
					async () => 
					{
						try
						{
							await msg.channel.send(dumpMessage)
						}
						catch(e){}
					},
					300
				)
				return
			}
		}
	)
}


