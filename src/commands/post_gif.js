const http = require('./../http')
const fs = require('fs')


async function run(msg, channel, args)
{
	var gifs
	await fs.readFile(
		"src/commands/gifs.txt", 
		'utf8', 
		async function(err, data)
		{
			gifs = data.split(/\r?\n/)
			var gif = gifs[Math.floor(gifs.length * Math.random())]
		
			console.log("Sending gif: " + gif)
			await channel.send(gif)
		}
	)
}

exports.run = run
