const http = require('./../http')

async function run(msg, channel, args)
{
	http.getHttps(
		"https://some-random-api.ml/facts/fox",
		async d => {
			var fact = JSON.parse(d).fact
			console.log("Sending fact: " + fact)
			await msg.channel.send(fact)
		}
	)
}

exports.run = run