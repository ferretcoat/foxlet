const http = require('./../http')
const fs = require('fs')


async function run(msg, channel, args)
{
	var gondolas
	await fs.readFile(
		"src/commands/gondolas.txt", 
		'utf8', 
		async function(err, data)
		{
			gondolas = data.split(/\r?\n/)
			var gondola = gondolas[Math.floor(gondolas.length * Math.random())]
		
			console.log("Sending gondola: " + gondola)
			await channel.send(gondola)
		}
	)
}

exports.run = run
