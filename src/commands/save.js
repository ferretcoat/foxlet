const http = require('./../http')
const fs = require('fs')
const client = require('https')
const path = require('path')

async function run(msg, channel, args)
{
	if (msg.author.id != process.env.OWNER)
	{
		msg.channel.send("You are not my mother! However, you can still view the meme archive at " + process.env.PUBLIC_MEME_LINK)
		return
	}

	var urls = null
	var urlsFromReply = false
	console.log(msg.reference)

	if (msg.reference)
	{
		var reply = await msg.fetchReference()
		
		urls = getUrlsFromMessage(reply)
		if (urls.length > 0)
		{
			urlsFromReply = true
		}
	}

	if (!urlsFromReply)
	{
		urls = getUrlsFromMessage(msg)
	}
	var memeName = ""

	if (args[0] && !args[0].includes("https://"))
	{
		memeName = args[0].replace(/[^a-zA-Z0-9._\/]/ig, '_') + "_"
	}

	if (urls.length > 0)
	{
		var filenames = []

		for(var i = 0; i < urls.length; i += 1)
		{
			var filename = memeName + Date.now() + path.extname(processUrl(urls[i]))

			filenames.push(filename);
			if (isPicture(filename))
			{
				filename = "pic/" + filename
			}
			if (isGif(filename))
			{
				filename = "gif/" + filename
			}
			if (isVideo(filename))
			{
				filename = "vid/" + filename
			}

			var dirName = process.env.MEME_DIR + path.dirname(filename)

			if (!fs.existsSync(dirName))
			{
				fs.mkdirSync(dirName)
			}

			await downloadFile(urls[i], process.env.MEME_DIR + filename)
		}
		var savedMessage = await msg.channel.send("Saved `" + filenames.join(", ") + "` to the vault!")

		setTimeout(
			async () => 
			{
				try
				{
					await savedMessage.delete()
				}
				catch(e){}
			}, 
			5000
		)

		if (urlsFromReply)
		{
			setTimeout(
				async () => 
				{
					try
					{
						await msg.delete()
					}
					catch(e){}
				}, 
				5000
			)
		}

	}
	else
	{
		await msg.channel.send("No attachments!\nMeme archive at: " + process.env.PUBLIC_MEME_LINK);
	}
}

function getUrlsFromMessage(msg)
{
	var urls = []
	getUrlsFromAttachments(msg, urls)
	getUrlsFromText(msg.content, urls)
	return urls
}

function getUrlsFromAttachments(msg, urls)
{
	if (msg.attachments && msg.attachments.size > 0)
	{
		var attachments = msg.attachments.values();
		console.log(attachments)

		for (let [key, value] of msg.attachments)
		{
			if (isSupportedFormat(processUrl(value.url)))
			{
				urls.push(value.url)
			}
		}
	}
	return urls
}

function getUrlsFromText(messageContent, urls)
{
	if (messageContent.includes("https://"))
	{
		words = messageContent.replace("\n", " ").split(" ")
		for(var i = 0; i < words.length; i += 1)
		{
			var processedUrl = processUrl(words[i]);
			if (words[i].includes("https://") && isSupportedFormat(processedUrl))
			{
				urls.push(words[i])
			}
		}
	}
}

function processUrl(url)
{
	return url.split('?')[0].split('&')[0]
}

function isSupportedFormat(filename)
{
	return isPicture(filename) || isVideo(filename) || isGif(filename)
}
function isPicture(filename)
{
	filename = filename.toLowerCase()
	return filename.endsWith(".png") || filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".webp")
}
function isVideo(filename)
{
	filename = filename.toLowerCase()
	return filename.endsWith(".mov") || filename.endsWith(".mp4") || filename.endsWith(".webm")
}
function isGif(filename)
{
	filename = filename.toLowerCase()
	return filename.endsWith(".gif")// || filename.startsWith("https://tenor.com")
}


function downloadFile(url, filepath) 
{
	return new Promise(
		(resolve, reject) => 
		{
			client.get(
				url, 
				(res) => 
				{
					if (res.statusCode === 200) 
					{
						res.pipe(fs.createWriteStream(filepath))
							.on('error', reject)
							.once('close', () => resolve(filepath))
					} 
					else
					{
						// Consume response data to free up memory
						res.resume();
						reject(new Error(`Request Failed With a Status Code: ${res.statusCode}`))
					}
				}
			)
		}
	)
}

exports.run = run