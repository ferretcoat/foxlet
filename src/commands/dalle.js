const Discord = require('discord.js')
const http = require('https')
const fs = require('fs')

var busy = false

async function run(msg, channel, args) 
{
	if (args === null || args === undefined)
	{
		return
	}

	var prompt = args.join(" ")


	if (msg.author.id !== "855155221448884324" && msg.channel.id !== "985297417656209448" && msg.guild.id === "699037523501908049")
	{
		channel.send("This command only works in <#985297417656209448> lol fuck you.")
		return
	}

	if (prompt == undefined || prompt == "")
	{
		channel.send("I can't draw anything to that prompt!")
		return
	}

	if (msg.author.id == process.env.OWNER || !busy)
	{
		busy = true
		setTimeout(
			() => busy = false,
			5000
		)
		console.log("DALLE START!")
		await msg.channel.send("Oh my, " + prompt + " gives me so much inspiration! Give me a couple minutes to draw it. You can call this command again in 5 seconds.")
		internalRun(msg, channel, prompt, 0)
	}
	else
	{
		await msg.channel.send("I'm on cooldown! Wait a little bit and try again.")
	}
}

async function internalRun(msg, channel, prompt, retryCount) 
{
	console.log("Sending stuff!")
	send(
		channel,
		prompt,
		async res => 
		{
			console.log(`STATUS: ${res.statusCode}`)

			if (res.statusCode != 200)
			{
				console.log("REQUEST FAILED: " + res.statusCode + " Retrying for " + (retryCount + 1) + "th time...")

				if (retryCount > 32)
				{
					console.log("Too many retries! Aborting.")
					await channel.send("I don't feel like drawing " + prompt + " :(")
					//busy = false
					return
				}
				setTimeout(
					async () => await internalRun(msg, channel, prompt, retryCount + 1), 
					2000
				)
			}
			else
			{
				res.setEncoding('utf8')
				var images = ""
				res.on(
					'data', 
					async (chunk) => 
					{
						images += chunk
					}
				);

				res.on('end', async () => {
					console.log('SUCCESS!');
					console.log(images)

					var img = JSON.parse(images).images

					var attaches = []
					for(var i = 0; i < img.length; i += 1)
					{
						const sfbuff = new Buffer.from(img[i].replace("\n", ""), "base64");

						fs.createWriteStream(
							process.env.VAULT + "dalle/" + (Date.now() + "_" + i + ".png").replace(" ", "_")
						).write(sfbuff);

						const sfattach = new Discord.AttachmentBuilder(sfbuff, {name: "output" + i + ".png"});
						attaches.push(sfattach)
					}

					await channel.send(
						{ 
							content: "Here's your " + prompt + ". Enjoy!", 
							reply: { messageReference: msg.id }, 
							files: attaches 
						}
					);


					busy = false
				});
			}
		}
	)
}

//run(null, null, null)

exports.run = run


function send(channel, prompt, callback)
{
	const postData = JSON.stringify(
		{
			'prompt': prompt
		}
	)

	const options = {
		hostname: 'backend.craiyon.com',//'bf.dallemini.ai',
		port: 443,//80,
		path: '/generate',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Content-Length': Buffer.byteLength(postData)
		}
	}

	const req = http.request(
		options, 
		callback
	)

	req.on(
		'error', 
		async (e) => 
		{
			await channel.send("I don't feel like drawing " + prompt + ". :(")
			await channel.send("error: " + e)
			//busy = false
		}
	)

	// Write data to request body
	req.write(postData)
	req.end()
}