const http = require('./../http')
const cheerio = require('cheerio')

async function run(msg, channel, args)
{
	http.getHttp(
		"http://bash.org/?random",
		async d => {
			var h = cheerio.load(d)
			var quotes = h("p.qt")

			var quoteId = Math.floor(quotes.length * Math.random())
			var original = quotes[quoteId].children[0].data
			var quote = stripQuote(original)
			
			console.log("Shitposting: " + quote)
			console.log("Original: " + original)
			
			if (args != null && args.length == 2 && args[0] && args[1] !== undefined)
			{
				quote = args[1] + " " + quote
			}

			await channel.send(quote)
		}
	)
}

function stripQuote(quote)
{
	var regex = /(?:<.+?>|\(.+?\)|<.+?>\:|\(.+?\)\:|.+?:)/
	return quote.replace(":", "").replace(regex.exec(quote), "").trim()
}

exports.run = run
