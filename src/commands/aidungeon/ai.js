const aidungeon = require('./aidungeon')

async function run(msg, channel, args)
{
	await channel.send("FOXLET DUNGEON isn't functioning right now. :(")
	return
	// TODO: Fix when someone will post proper login routine.

	if (args == null || args.length == 0 || args[0] === "")
	{
		await help(channel)
		return
	}

	action = args[0]
	if (action === "do")
	{
		await sendAction(channel, assembleMessage(args), "do")
		return
	}
	if (action === "say")
	{
		await sendAction(channel, assembleMessage(args), "say")
		return
	}
	if (action === "story")
	{
		await sendAction(channel, assembleMessage(args), "story")
		return
	}
	if (action === "start")
	{
		await start(channel, assembleMessage(args))
		return
	}
	
	await help(channel)
}

exports.run = run


async function help(channel)
{
	await channel.send(`
FOXLET DUNGEON ALPHA
!ai start <prompt> - start new adventure
!ai do <message> - perform an action
!ai say <message> - say something
!ai story <message> - advance story
`)
}

async function start(channel, message)
{
	await aidungeon.createAnonUser()

	var content = await aidungeon.createAdventure(message)

	if (content === null)
	{
		await channel.send("**You need to wait 60 seconds before creating new adventure!**")
		return
	}

	console.log(JSON.stringify(content, undefined, 2))

	await channel.send(
		content.historyList[0].text +
		content.historyList[1].text
	)
}

async function sendAction(channel, message, actionType)
{
	var content = await aidungeon.sendAction(message, actionType)
	if (content === null)
	{
		await channel.send("**You need to wait 1 second before sending another action!**")
		return
	}

	console.log(JSON.stringify(content, undefined, 2))

	var lastId = content.historyList.length - 1
	var message = content.historyList[lastId - 1].text + content.historyList[lastId].text

	if (message === "")
	{
		await channel.send("AI doesn't know what to say.")
	}
	else
	{
		await channel.send(message)
	}
}

function assembleMessage(args)
{
	var message = ""
	for(var i = 1; i < args.length; i += 1)
	{
		message += args[i] + " "
	}
	return message
}