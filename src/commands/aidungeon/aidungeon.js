var { request, gql, GraphQLClient } = require('graphql-request')

const accountQuery = gql`
mutation 
{  
	createAnonymousAccount 
	{   
		id    
		accessToken    
		__typename  
	}
}
`

const scenarioQuery = gql`
mutation 
{
	createNewScenario
	{
		id
		__typename
	}
}
`

const adventureQuery = gql`
mutation(
	$id: String
	$prompt: String
	$freestyle: Boolean
) 
{  
	createAdventureFromScenarioId(
		id: $id
		prompt: $prompt
		freestyle: $freestyle
	)
  {
		id
		prompt
		contentType
		contentId
		title
		description
		tags
		nsfw
		published
		publicId
		historyList
		__typename
	}
}
`

const sendActionQuery = `
mutation ($input: ContentActionInput) 
{
	sendAction(input: $input) 
	{
		id
		actionLoading    
		memory    
		died    
		gameState   
		historyList
		__typename 
	}
}
`

const client = new GraphQLClient('http://api.aidungeon.io/graphql')

var accessToken

async function createAnonUser()
{
	var data = await client.request(accountQuery)
	console.log(data)
	accessToken = data.createAnonymousAccount.accessToken
	console.log(accessToken)
	client.setHeader("x-access-token", data.createAnonymousAccount.accessToken)
}

async function createAdventure(prompt)
{
	if (adventureTimeoutActive)
	{
		return null
	}
	adventureTimeoutActive = true
	setTimeout(() => adventureTimeoutActive = false, 1 * 60 * 1000)

	var scenario = await client.request(scenarioQuery)
	console.log(scenario)
	var adventureVariables = 
	{
		id: scenario.createNewScenario.id,
		prompt: prompt,
		freestyle: true
	}
	var adventure = await client.request(adventureQuery, adventureVariables)
	adventureId = adventure.createAdventureFromScenarioId.id

	console.log(JSON.stringify(adventure, undefined, 2))

	return adventure.createAdventureFromScenarioId
}

async function sendAction(action, actionType)
{
	if (messageTimeoutActive)
	{
		return null
	}
	messageTimeoutActive = true
	setTimeout(() => messageTimeoutActive = false, 1 * 1 * 1000)

	var variables = 
	{
		input:
		{
			text: action,
			type: actionType,
			id: adventureId
		}
	}

	var content = await client.request(sendActionQuery, variables)
	console.log(JSON.stringify(content, undefined, 2))

	return content.sendAction
}


async function dbg()
{
	await createAnonUser()
	await createAdventure("You are the wind foxe, you live in the forest and encounter a")
	await sendDo("suck cock")
}

//dbg()

var adventureTimeoutActive = false
var messageTimeoutActive = false

var loggedIn

exports.createAnonUser = createAnonUser
exports.createAdventure = createAdventure
exports.sendAction = sendAction

exports.loggedIn = loggedIn