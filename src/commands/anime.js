const Discord = require('discord.js')
const http = require('https')
const fs = require('fs')

var busy = false

async function run(msg, channel, args) 
{
	if (args === null || args === undefined)
	{
		return
	}

	if (msg.author.id !== "855155221448884324" && msg.channel.id !== "985297417656209448" && msg.guild.id === "699037523501908049")
	{
		channel.send("This command only works in <#985297417656209448> lol fuck you.")
		return
	}

	if (msg.author.id == process.env.OWNER || !busy)
	{
		console.log("ANIME START!")
		var urls = getUrlsFromMessage(msg)
		if (urls.length > 0)
		{
			busy = true
			setTimeout(
				() => busy = false,
				15000
			)
			await channel.send(
				{ 
					content: "On it! You can use this command again in 15 seconds.", 
					reply: { messageReference: msg.id }, 
				}
			);
			http.get(urls[0], 
				(resp) => 
				{
					resp.setEncoding('base64')
					body = ""
					resp.on('data', (data) => { body += data })
					resp.on('end', () => {internalRun(msg, channel, body, 0)})
				}
			).on(
				'error', 
				(e) => {console.log(`Got error: ${e.message}`)}
			)
		}
		else
		{
			await msg.channel.send("No attachments! Attach an image or an image url to your message. Make sure the url ends with an image extension.")
		}
	}
	else
	{
		await msg.channel.send("I'm on cooldown! Wait 10 seconds and try again.")
	}
}


async function internalRun(msg, channel, imgData, retryCount) 
{
	console.log("Sending stuff!")
	send(
		channel,
		imgData,
		async res => 
		{
			console.log(`STATUS: ${res.statusCode}`)

			if (res.statusCode != 200)
			{
				console.log("REQUEST FAILED: " + res.statusCode + " Retrying for " + (retryCount + 1) + "th time...")

				if (retryCount > 32)
				{
					console.log("Too many retries! Aborting.")
					await channel.send("I don't feel like drawing :(")
					//busy = false
					return
				}
				setTimeout(
					async () => await internalRun(msg, channel, prompt, retryCount + 1), 
					2000
				)
			}
			else
			{
				res.setEncoding('utf8')
				var images = ""
				res.on(
					'data', 
					async (chunk) => 
					{
						images += chunk
					}
				);

				res.on('end', async () => {
					console.log('SUCCESS!');
					console.log(images)
					var response = JSON.parse(images)

					if (response.code == "0")
					{
						var imgResult = JSON.parse(response.extra).img_urls[2]
						http.get(imgResult, 
							(resp) => 
							{
								resp.setEncoding('base64')
								body = ""
								resp.on('data', (data) => { body += data })
								resp.on('end', 
									async () => 
									{
						
										var attaches = []
										const sfbuff = new Buffer.from(body.replace("\n", ""), "base64");
										const sfattach = new Discord.AttachmentBuilder(sfbuff, {name: "anime.png"});
										attaches.push(sfattach)

										await channel.send(
											{ 
												content: "Here's your anime. Enjoy!", 
												reply: { messageReference: msg.id }, 
												files: attaches 
											}
										);
									}
								)
							}
						).on(
							'error', 
							(e) => {console.log(`Got error: ${e.message}`)}
						)
					}
					else
					{
						if (response.code == "2114")
						{
							await channel.send(
								{ 
									content: "🛑 WARNING! This image has been deemed unacceptable by the 🇨🇳 Chinese Communist Party ⭐. This incident will be reported to Supreme Leader Xi. An appropriate amount of Social Credit will be deducted. Glory to CCP!",
									reply: { messageReference: msg.id }, 
								}
							);
						}
						else
						{
							if (response.code == "2111" || response.code == "-2110")
							{
								await channel.send(
									{ 
										content: "Ratelimited. Try again in a couple minutes. :( ",
										reply: { messageReference: msg.id }, 
									}
								);
							}
							else
							{
								await channel.send(
									{ 
										content: "I shat my pants. :( \n Error code: " + response.code + "\nMessage: " + response.msg,
										reply: { messageReference: msg.id }, 
									}
								);
							}
						}
					}

					/*
					var img = JSON.parse(images).images

					var attaches = []
					for(var i = 0; i < img.length; i += 1)
					{
						const sfbuff = new Buffer.from(img[i].replace("\n", ""), "base64");

						const sfattach = new Discord.AttachmentBuilder(sfbuff, {name: "output" + i + ".png"});
						attaches.push(sfattach)
					}*/

					busy = false
				});
			}
		}
	)
}

//run(null, null, null)

exports.run = run

function send(channel, imageData, callback)
{
	const guid = uuidv4()
	console.log(guid)
	const postData = JSON.stringify(
		{
			"busiId": "ai_painting_anime_img_entry",
			"images": 
			[
				imageData,
			],
			"extra": "{\"face_rects\":[],\"version\":2,\"platform\":\"web\",\"data_report\":{\"parent_trace_id\":\"" + guid + "\",\"root_channel\":\"\",\"level\":0}}"
		}
	)

	const options = 
	{
		hostname: 'ai.tu.qq.com',
		port: 443,//80,
		path: '/trpc.shadow_cv.ai_processor_cgi.AIProcessorCgi/Process',
		method: 'POST',
		headers: 
		{
			'Content-Type': 'application/json',
			'Content-Length': Buffer.byteLength(postData),
			'User-Agent': "FoxletRuntime/" + Math.random() * 100 + "." + Math.random() * 100 + "." + Math.random() * 100 // Hopefully this gets around the ratelimiter.
		}
	}

	const req = http.request(
		options, 
		callback
	)

	req.on(
		'error', 
		async (e) => 
		{
			await channel.send("I don't feel like drawing this image. :(")
			await channel.send("error: " + e)
			//busy = false
		}
	)

	// Write data to request body
	req.write(postData)
	req.end()
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}


function getUrlsFromMessage(msg)
{
	var urls = []
	getUrlsFromAttachments(msg, urls)
	getUrlsFromText(msg.content, urls)
	return urls
}

function getUrlsFromAttachments(msg, urls)
{
	if (msg.attachments && msg.attachments.size > 0)
	{
		var attachments = msg.attachments.values();
		console.log(attachments)

		for (let [key, value] of msg.attachments)
		{
			if (isImage(value.url))
			{
				urls.push(value.url)
			}
		}
	}
	return urls
}

function getUrlsFromText(messageContent, urls)
{
	if (messageContent.includes("https://"))
	{
		words = messageContent.replace("\n", " ").split(" ")
		for(var i = 0; i < words.length; i += 1)
		{
			if (words[i].includes("https://") && isImage(words[i]))
			{
				urls.push(words[i])
			}
		}
	}
}

function isImage(filename)
{
	return filename.toLowerCase().endsWith(".png") || filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".jpeg")
}