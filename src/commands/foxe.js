const http = require('./../http')

async function run(msg, channel, args)
{
	http.getHttps(
		"https://some-random-api.ml/img/fox",
		async d => {
			var pic = JSON.parse(d).link
			console.log("Sending image: " + pic)
			await channel.send(pic)
		}
	)
}

exports.run = run