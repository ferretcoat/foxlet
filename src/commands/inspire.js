const http = require('../http')

async function run(msg, channel, args)
{
	http.getHttps(
		"https://inspirobot.me/api?generate=true",
		async d => {
			console.log("Inspiring: " + d)

			await channel.send(d)
		}
	)
}

//run(null, null, null)

exports.run = run