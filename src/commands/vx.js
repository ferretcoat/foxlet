const http = require('./../http')
const fs = require('fs')
const client = require('https')
const path = require('path')

async function run(msg, channel, args)
{
	if (msg.author.id != process.env.OWNER)
	{
		msg.channel.send("You are not my mother! However, you can still view the meme archive at " + process.env.PUBLIC_MEME_LINK)
		return
	}

	var urls = null
	var urlsFromReply = false
	console.log(msg.reference)

	if (msg.reference)
	{
		var reply = await msg.fetchReference()

		if (reply.content.includes("https://twitter.com"))
		{
			fixTwitter(reply, reply.content)
			return;
		}
	}

	await msg.channel.send("No twitter link found!");
}

function fixTwitter(msg, messageContent)
{
	if (messageContent.includes("https://twitter.com"))
	{
		replacedLinks = "Since you are TOO STUPID to use vxtwitter:\n"
		words = messageContent.replace("\n", " ").split(" ")
		for(var i = 0; i < words.length; i += 1)
		{
			if (words[i].includes("https://twitter.com"))
			{
				replacedLinks += words[i].replace("https://twitter.com", "https://vxtwitter.com") + "\n"
			}
		}

		setTimeout(
			async () => 
			{
				try
				{
					await msg.channel.send({ content: replacedLinks, reply: { messageReference: msg.id } });
				}
				catch(e){}
			}, 
			100
		)
	}
}

exports.run = run