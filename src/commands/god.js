const fs = require('fs')
const discord = require('discord.js');

var words = null

async function run(msg, channel, args)
{

	var wordCount = parseInt(args[0])
	console.log(wordCount + " " + args[0])

	setSeed((Date.now() + "GOD").hashCode())

	var useSeed = false
	if (args != undefined && args.length > 0)
	{
		if (args.length != 1 || isNaN(wordCount))
		{
			var message = ""
			for(var i = 0; i < args.length; i += 1)
			{
				message += args[i] + " "
			}

			setSeed(message.hashCode())
			useSeed = true
		}
	}

	if (isNaN(wordCount) || wordCount <= 0 || wordCount > 100)
	{
		wordCount = 1 + Math.floor(32 * seededRandom())//skewRng(32)
	}

	if (words == null)
	{
		console.log("Loading GOD'S WORD for the first time...")
		var contents = fs.readFileSync(
			"src/commands/bible.txt",
			'utf8'
		)
		words = contents.split(/\r?\n/)

		console.log("Finished loading!")
	}
	
	var phrase = ""
	for(var i = 0; i < wordCount; i += 1)
	{
		var r = seededRandom()
		if (useSeed)
		{
			r = seededRandom()
		}
		phrase += words[Math.floor(words.length * r)] + " "
	}

	console.log("GOD SAYS:" + phrase)

	var embed = new discord.EmbedBuilder()
		.setColor('#FF7F00')
		.setTitle("GOD SAYS")
		.setDescription(phrase)
		.setFooter({ text: 'powered by autism' })

	await channel.send({ embeds: [embed] })
}

function skewRng(maxValue)
{
	var r = Math.random()
	return Math.floor(maxValue * r * r)
}

exports.run = run

var m_w = 123456789;
var m_z = 987654321;
var mask = 0xffffffff;

function setSeed(seed)
{
	m_w = (123456789 + seed) & mask;
	m_z = (987654321 - seed) & mask;
}

function seededRandom()
{
		m_z = (36969 * (m_z & 65535) + (m_z >> 16)) & mask;
		m_w = (18000 * (m_w & 65535) + (m_w >> 16)) & mask;
		var result = ((m_z << 16) + (m_w & 65535)) >>> 0;
		result /= 4294967296;
		return result;
}

String.prototype.hashCode = function() 
{
	var hash = 0;
	for (var i = 0; i < this.length; i++) 
	{
			var char = this.charCodeAt(i);
			hash = ((hash<<5)-hash)+char;
			hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}