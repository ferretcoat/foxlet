const http = require('http')
const https = require('https')

var StringDecoder = require('string_decoder').StringDecoder;

function get(protocol, url, action)
{
	const req = protocol.get(
		url, 
		res => {
			console.log(`statusCode: ${res.statusCode}`)
			
			var httpData = ""

			res.on(
				'data', 
				chunk => {
					httpData += chunk
				}
			)

			res.on(
				'end', 
				() => {
					var decoder = new StringDecoder('utf8');
					action(decoder.write(httpData))
				}
			)
			
		}
	)
	
	req.on(
		'error', 
		error => {
			console.error(error)
		}
	)
	
	req.end()
}



function getHttp(url, action)
{
	return get(http, url, action)
}
function getHttps(url, action)
{
	return get(https, url, action)
}


exports.getHttp = getHttp
exports.getHttps = getHttps