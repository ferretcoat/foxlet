const http = require('../http')
const fs = require('fs')
const client = require('https')
const path = require('path')
const config = require('./config')

async function run(client)
{
	try
	{
		var files = await readFiles()

		console.log(files)

		var channel = client.channels.cache.get(config.getProtectedThreadChannel())
		var message = await channel.send("restoring...")

		var thread = await message.startThread(
			{
				name: 'backup restore',
				autoArchiveDuration: 60,
				type: 'GUILD_PUBLIC_THREAD',
				reason: 'test'
			}
		);
		config.updateThreadId(thread.id)

		await restoreContent(files, thread, client)

		console.log(client.channels.cache.get(config.getProtectedThreadChannel()).id)
	}
	catch(e)
	{
		console.log("RESTORE FAILED!" + e)
		var user = await client.users.fetch(process.env.OWNER)
		await user.send(`Restore FAILED! ` + e)
	}
}

async function restoreContent(files, thread, client)
{
	try
	{
		for(var i = 0; i < files.length; i += 1)
		{
			if (files[i].endsWith(".txt"))
			{
				var url = fs.readFileSync(files[i], 'utf8')
				await thread.send(url)
			}
			else
			{
				await thread.send({
					files: [
						files[i]
					]
				});
			}
			await setTimeout(()=>{}, 200)
		}

		var user = await client.users.fetch(process.env.OWNER)
		await user.send(`Restore complete!`)
	}
	catch(e)
	{
		console.log("RESTORE FAILED!" + e)
		var user = await client.users.fetch(process.env.OWNER)
		await user.send(`Restore FAILED! ` + e)
	}
}

async function readFiles()
{
	const getSortedFiles = async (dir) => 
	{
		const files = await fs.promises.readdir(dir);
	
		return files
			.map(fileName => ({
				name: fileName,
				time: fs.statSync(`${dir}/${fileName}`).mtime.getTime(),
			}))
			.sort((a, b) => a.time - b.time)
			.map(file => config.getProtectedThreadArchiveDir() + file.name);
	};
	return await getSortedFiles(config.getProtectedThreadArchiveDir())
}


exports.run = run