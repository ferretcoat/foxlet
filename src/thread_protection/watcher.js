const config = require('./config')
const save = require('./save')
const restore = require('./restore')

async function init(client)
{
	client.on('threadDelete', async (thread) => {
		if (thread.id == config.getProtectedThread())
		{
			var user = await client.users.fetch(process.env.OWNER)
			await user.send(`FUCK! Protected thread ${thread.name} has been deleted. Trying to restore now...`)
			await restore.run(client)
		}
	})


	client.on(
		'messageCreate', 
		async msg => 
		{
			try
			{
				if (msg.author.bot)
				{
					return
				}
				if (msg.channel.id === config.getProtectedThread())
				{
					console.log("POSTING IN A PROTECTED THREAD!")
					save.run(msg)
				}
			}
			catch(e){}
		}
	)
}

exports.init = init

