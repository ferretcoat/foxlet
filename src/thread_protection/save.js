const http = require('../http')
const fs = require('fs')
const client = require('https')
const path = require('path')
const config = require('./config')

async function run(msg)
{
	var urls = null
	
	urls = getUrlsFromMessage(msg)

	if (urls.length > 0)
	{
		var filenames = []

		for(var i = 0; i < urls.length; i += 1)
		{
			var memeName = processUrl(urls[i]).split('/').pop()
			var filename = Date.now() + path.extname(processUrl(urls[i]))

			filenames.push(filename);

			var dirName = config.getProtectedThreadArchiveDir() + path.dirname(filename)

			if (!fs.existsSync(dirName))
			{
				fs.mkdirSync(dirName)
			}

			await downloadFile(urls[i], config.getProtectedThreadArchiveDir() + filename)
		}
	}

	var unsupportedUrls = getUnsupportedUrlsFromText(msg.content)

	if (unsupportedUrls.length > 0)
	{
		for(var i = 0; i < unsupportedUrls.length; i += 1)
		{
			var filename = Date.now() + ".txt"

			var dirName = config.getProtectedThreadArchiveDir() + path.dirname(filename)

			if (!fs.existsSync(dirName))
			{
				fs.mkdirSync(dirName)
			}
			fs.writeFileSync(config.getProtectedThreadArchiveDir() + filename, unsupportedUrls[i])
			console.log("Saved " + unsupportedUrls[i] + " to " + filename)
		}
	}

}

function getUrlsFromMessage(msg)
{
	var urls = []
	getUrlsFromAttachments(msg, urls)
	getUrlsFromText(msg.content, urls)
	return urls
}

function getUrlsFromAttachments(msg, urls)
{
	if (msg.attachments && msg.attachments.size > 0)
	{
		var attachments = msg.attachments.values();
		console.log(attachments)

		for (let [key, value] of msg.attachments)
		{
			urls.push(value.url)
		}
	}
	return urls
}

function getUrlsFromText(messageContent, urls)
{
	if (messageContent.includes("https://"))
	{
		words = messageContent.replace("\n", " ").split(" ")
		for(var i = 0; i < words.length; i += 1)
		{
			var processedUrl = processUrl(words[i]);
			if (words[i].includes("https://") && isSupportedFormat(processedUrl))
			{
				urls.push(words[i])
			}
		}
	}
}

function getUnsupportedUrlsFromText(messageContent)
{
	var urls = []
	if (messageContent.includes("https://"))
	{
		words = messageContent.replace("\n", " ").split(" ")
		for(var i = 0; i < words.length; i += 1)
		{
			var processedUrl = processUrl(words[i]);
			if (words[i].includes("https://") && !isSupportedFormat(processedUrl))
			{
				urls.push(words[i])
			}
		}
	}
	return urls
}

function processUrl(url)
{
	return url.split('?')[0].split('&')[0]
}

function isSupportedFormat(filename)
{
	return isPicture(filename) || isVideo(filename) || isGif(filename)
}
function isPicture(filename)
{
	filename = filename.toLowerCase()
	return filename.endsWith(".png") || filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".webp")
}
function isVideo(filename)
{
	filename = filename.toLowerCase()
	return filename.endsWith(".mov") || filename.endsWith(".mp4") || filename.endsWith(".webm")
}
function isGif(filename)
{
	filename = filename.toLowerCase()
	return filename.endsWith(".gif")// || filename.startsWith("https://tenor.com")
}


function downloadFile(url, filepath) 
{
	return new Promise(
		(resolve, reject) => 
		{
			client.get(
				url, 
				(res) => 
				{
					if (res.statusCode === 200) 
					{
						res.pipe(fs.createWriteStream(filepath))
							.on('error', reject)
							.once('close', () => resolve(filepath))
					} 
					else
					{
						// Consume response data to free up memory
						res.resume();
						reject(new Error(`Request Failed With a Status Code: ${res.statusCode}`))
					}
				}
			)
		}
	)
}

exports.run = run