const fs = require('fs')
var config = require('./config.json')

function updateThreadId(newId)
{
	var c = fs.readFileSync("src/thread_protection/config.json", 'utf-8')
	c = c.replace(config.protectedThread, newId)
	fs.writeFileSync("src/thread_protection/config.json", c, "utf-8")
	config.protectedThread = newId
}


function getProtectedThread()
{
	return config.protectedThread
}
function getProtectedThreadChannel()
{
	return config.protectedThreadChannel
}
function getProtectedThreadArchiveDir()
{
	return config.protectedThreadArchiveDir
}

exports.updateThreadId = updateThreadId
exports.getProtectedThread = getProtectedThread
exports.getProtectedThreadChannel = getProtectedThreadChannel
exports.getProtectedThreadArchiveDir= getProtectedThreadArchiveDir
